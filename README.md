# HeroesInterface

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Connect to [`localhost:4000`](http://localhost:4000) from two seperate browser windows 
to simulate two users. The changes made in one window should be reflected in the other 
in near real time, without having to refresh the browser.

# Specs

* Display a list of items
* View/Edit item
* Add new item

* Use Phoenix (to implement views)
* Save list in a json file
* Database not needed

# Implementation

The logic and state is handled by [`HeroesEngine`](https://gitlab.com/smedegaard/heroes_engine).

Phoenix has mainly been used for it's `Channel`s. Hence the most interesing file in this repo
is probably `/lib/heroes_interface_web/channels/heroes_channel.ex`.

I did *not* use the generated html templates and thus the only thing manipulating the
interface is javascript.

# Features

* Create a list
* Add items
* Delete items
* Edit items
* Save list to json, server-side

* More users can work on one list
  * Join active list
  * Manipulate and save list concurrently
* Fault tolerant
  * If the `HeroesEngine` crashes a new one with the last known good state is spun up.

# Documentation
Docs can be generated with `mix docs`

