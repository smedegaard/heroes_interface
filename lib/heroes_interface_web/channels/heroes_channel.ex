defmodule HeroesInterfaceWeb.HeroesChannel do
  use HeroesInterfaceWeb, :channel

  alias HeroesEngine.HeroesSupervisor
  alias HeroesEngine.Hero

  def join("heroes:lobby", _payload, socket) do
    send(self(), :joined_lobby)
    {:ok, socket}
  end

  def join("heroes:" <> _topic, _payload, socket) do
    {:ok, socket}
  end

  def handle_info(:joined_lobby, socket) do
    update_lists(socket)
  end

  def handle_in("list_created", _payload, socket) do
    update_lists(socket)
  end

  def handle_in("new_list", _payload, socket) do
    "heroes:" <> list_name = socket.topic

    case HeroesSupervisor.start_heroes_list(list_name) do
      {:ok, _pid} ->
        {:reply, :ok, socket}

      {:error, reason} ->
        {:reply, {:error, %{reason: inspect(reason)}}, socket}
    end
  end

  def handle_in("add_hero", %{"name" => name, "power" => power}, socket) do
    case HeroesEngine.add_hero(via(socket.topic), Hero.new(name, power)) do
      {:ok, {:hero_added, state}} ->
        list = heroes_to_list(state[:heroes])
        broadcast!(socket, "hero_added", %{message: "Hero added : " <> name, new_list: list})
        {:noreply, socket}

      {{:error, :hero_exsists}, state} ->
        {:reply, {:error, %{reason: "Hero exsists"}, heroes: state}}
    end
  end

  def handle_in("delete_hero", %{"name" => name}, socket) do
    case HeroesEngine.remove_hero(via(socket.topic), String.to_atom(name)) do
      {:ok, {:hero_removed, state}} ->
        list = heroes_to_list(state[:heroes])
        broadcast!(socket, "hero_removed", %{message: "Hero removed : " <> name, new_list: list})
        {:noreply, socket}

      {{:error, _}, state} ->
        {:reply, {:error, %{reason: "Could not remove hero $"}, heroes: state}}
    end
  end

  def handle_in("fetch_heroes", _payload, socket) do
    result = HeroesEngine.get_heroes(via(socket.topic))

    case result do
      {:ok, heroes} ->
        list = heroes_to_list(heroes)
        push(socket, "heroes_fetched", %{heroes: list})
        {:noreply, socket}

      _ ->
        push(socket, "heroes_fetched", %{heroes: []})
        {:noreply, socket}
    end
  end

  def handle_in(
        "update_hero",
        %{"old_name" => old_name, "new_name" => new_name, "power" => power},
        socket
      ) do
    result =
      HeroesEngine.update_hero(
        via(socket.topic),
        String.to_atom(old_name),
        Hero.new(new_name, power)
      )
    case result do
      {:ok, {:hero_updated, new_state}} ->
        heroes = heroes_to_list(new_state[:heroes])
        broadcast!(socket, "hero_updated", %{new_list: heroes})
        {:noreply, socket}

      _ ->
        push(socket, "hero_updated", %{message: "could not update"})
        {:noreply, socket}
    end
  end

  def handle_in("save_to_json", _payload, socket) do
    case HeroesEngine.get_heroes(via(socket.topic)) do
      {:ok, heroes} ->
        case heroes_to_json(heroes, socket.topic) do
          {:ok, :json_saved} ->
            push(socket, "json_saved", %{})
          _ ->
            push(socket, "could_not_save", %{})
        end
        {:noreply, socket}
      {:error, posix} ->
        push(socket, "could_not_save", %{reason: posix})
        {:noreply, socket}
    end
  end

  @doc """
  Takes a `map` of heroes and saves the writes them to a json file
  at `/heroes_interface/heroes-data/<list_name>.json`.

  Returns {:ok, :json_saved} on success and {:error, {reason: posix}} if it fails.
  """
  def heroes_to_json(heroes_map, topic) do
    {:ok, json} =
      heroes_map
      |> heroes_to_list()
      |> Jason.encode()

    path = Path.absname("heroes-data")
    if not File.exists?(path) do
      File.mkdir_p(path)
    end
    # remove colon from file name . WINDOWS
    filename = String.replace(topic, ",", "-")
    case File.write(Path.absname("heroes-data/" <> filename <> ".json"), json) do
      :ok ->
        {:ok, :json_saved}
      {:error, posix} ->
        {:error, %{reason: posix}}
    end
  end

  @doc """
  Given a channel name, returns a tuple that the `Registry` of `HeroesEngine`
  can use to idetify the GenServer holding the heroes list.
  """
  defp via("heroes:" <> list_name), do: HeroesEngine.via_tuple(list_name)

  @doc """
  Convinience function for updating the list of active heroes list.

  Used when joining the lobby and when a new list is created.
  """
  defp update_lists(socket) do
    lists = HeroesSupervisor.get_children_names()
    broadcast!(socket, "update_lists", %{active_lists: lists})
    {:noreply, socket}
  end

  @doc """
  Converts a map containing `%Hero{}` struct to a list of maps.

  Usefull for conversion to JSON.
  """
  defp heroes_to_list(heroes_map) do
    case heroes_map do
      nil ->
        []

      _ ->
        Map.to_list(heroes_map)
        |> Enum.map(fn {_key, hero} -> Map.from_struct(hero) end)
    end
  end
end
