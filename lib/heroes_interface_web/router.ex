defmodule HeroesInterfaceWeb.Router do
  use HeroesInterfaceWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", HeroesInterfaceWeb do
    pipe_through :browser

    get "/", PageController, :index

    post "/add-hero", PageController, :add_hero
  end

  # Other scopes may use custom stacks.
  # scope "/api", HeroesInterfaceWeb do
  #   pipe_through :api
  # end
end
