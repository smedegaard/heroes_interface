defmodule HeroesInterfaceWeb.PageController do
  use HeroesInterfaceWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def add_hero(conn, %{"name" => name, "power" => power}) do
    {:ok, _pid} = HeroesEngine.start_link([])
    conn
    |> put_flash(:info, "Name : " <> name <> " | Power : " <> power)
    |> render("index.html")
  end
end
