// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// To use Phoenix channels, the first step is to import Socket,
// and connect at the socket path in "lib/web/endpoint.ex".
//
// Pass the token on params as below. Or remove it
// from the params if you are not using authentication.
import {Socket} from "phoenix"

let socket = new Socket("/socket", {params: {token: window.userToken}})

// When you connect, you'll often need to authenticate the client.
// For example, imagine you have an authentication plug, `MyAuth`,
// which authenticates the session and assigns a `:current_user`.
// If the current user exists you can assign the user's token in
// the connection for use in the layout.
//
// In your "lib/web/router.ex":
//
//     pipeline :browser do
//       ...
//       plug MyAuth
//       plug :put_user_token
//     end
//
//     defp put_user_token(conn, _) do
//       if current_user = conn.assigns[:current_user] do
//         token = Phoenix.Token.sign(conn, "user socket", current_user.id)
//         assign(conn, :user_token, token)
//       else
//         conn
//       end
//     end
//
// Now you need to pass this token to JavaScript. You can do so
// inside a script tag in "lib/web/templates/layout/app.html.eex":
//
//     <script>window.userToken = "<%= assigns[:user_token] %>";</script>
//
// You will need to verify the user token in the "connect/3" function
// in "lib/web/channels/user_socket.ex":
//
//     def connect(%{"token" => token}, socket, _connect_info) do
//       # max_age: 1209600 is equivalent to two weeks in seconds
//       case Phoenix.Token.verify(socket, "user socket", token, max_age: 1209600) do
//         {:ok, user_id} ->
//           {:ok, assign(socket, :user, user_id)}
//         {:error, reason} ->
//           :error
//       end
//     end
//
// Finally, connect to the socket:
socket.connect()

let lobby = socket.channel("heroes:lobby")
lobby
    .on("update_lists", resp => {
        show_active_lists(resp.active_lists)
    })


// Now that you are connected, you can join channels with a topic:
let error = document.querySelector("#error")
let current_channel = null

let newListSection = document.querySelector("#new-list-section")
let newButton = document.querySelector("#submit-topic")
let topicInput = document.querySelector("#topic-input")

let listSection = document.querySelector("#list-section")
let listTitle = document.querySelector("#list-title")

let heroNameInput = document.querySelector("#name-input")
let heroPowerInput = document.querySelector("#power-input")
let submitHeroButton = document.querySelector("#submit-hero")
let heroesList = document.querySelector("#heroes-list")

let joinListSection = document.querySelector("#join-list-section")
let activeListsContainer = document.querySelector("#active-lists")

let saveButton = document.querySelector("#save-button")

function new_channel(subtopic) {
    let channel = socket.channel("heroes:" + subtopic)

    channel.on("hero_removed", resp => {
        let heroes = resp.new_list
        refresh_heroes_list(channel, heroes)
    })

    channel.on("hero_added", resp => {
        let heroes = resp.new_list
        refresh_heroes_list(channel, heroes)
    })

    channel.on("heroes_fetched", resp => {
        refresh_heroes_list(channel, resp.heroes)
    })

    channel.on("hero_updated", resp => {
        refresh_heroes_list(channel, resp.new_list)
    })

    channel.on("json_saved", resp => {
        error.innerHTML = "Data saved as json"
        error.classList.remove("hidden")
        window.setTimeout(() => error.classList.add("hidden"), 7000)
    })

    current_channel = channel
    return channel
}

function join_channel(channel, topic) {
    channel.on("heroes_fetched", resp => {
        refresh_heroes_list(channel, resp.heroes)
    })

    channel.on("hero_removed", resp => {
        let heroes = resp.new_list
        refresh_heroes_list(channel, heroes)
    })

    channel.on("hero_added", resp => {
        let heroes = resp.new_list
        refresh_heroes_list(channel, heroes)
    })

    channel.on("hero_updated", resp => {
        refresh_heroes_list(channel, resp.new_list)
    })

    channel.on("json_saved", resp => {
        error.innerHTML = "Data saved as json"
    })

    channel.join()
        .receive("ok", resp => {
            if(topic != "lobby") {
                channel.push("fetch_heroes")
                show_list(topic)
                current_channel = channel
                console.log("Joined", resp)
            }
        })
        .receive("error", resp => { show_error(resp.reason) })
}

function new_list(topic) {
    let home = lobby
    let channel = new_channel(topic)
    join_channel(channel, topic)
    channel.push("new_list")
        .receive("ok", resp => {
            lobby.push("list_created")
            show_list(topic)
        })
        .receive("error", resp => {
            show_error(resp.reason)
        })
}

function add_hero(channel, name, power) {
    channel.push("add_hero", {name: name, power: power})
        .receive("error", resp => {
            show_error(resp.reason)
        })
}

function delete_hero(channel, name) {
    channel.push("delete_hero", {name: name})
        .receive("error", resp => {
            show_error(resp.reason)
        })
}

function update_hero(channel, old_name, new_name, power) {
    channel.push("update_hero", {old_name: old_name, new_name: new_name, power: power})
        .receive("ok", resp => {
            refresh_heroes_list(channel, resp.new_list)
        })
        .receive("error", resp => {
            show_error(resp.reason)
        })
}


function show_active_lists(active_lists) {
    activeListsContainer.innerHTML = ""
    if(active_lists.length == 0) {
        activeListsContainer.innerHTML = "No active lists"
        return
    }
    active_lists.forEach( name_of_list => {
        let listContainer = document.createElement("div")
        listContainer.classList.add("row")
        let listName = document.createElement("a")
        listName.addEventListener("click", event => {
            let channel = socket.channel("heroes:" + name_of_list)
            join_channel(channel, name_of_list)
        })
        listName.innerHTML = name_of_list
        listContainer.appendChild(listName)
        activeListsContainer.appendChild(listContainer)
    })
}

function show_list(topic) {
    newListSection.classList.add("hidden")
    joinListSection.classList.add("hidden")
    error.classList.add("hidden")
    listSection.classList.remove("hidden")
    listTitle.innerText = topic + "-list"
}

function show_error(reason) {
    if (reason.startsWith("{:already_started")) {
        error.innerHTML = "That list already exsists"
    } else {
        error.innerHTML = "Something went wrong"
    }
}

function refresh_heroes_list(channel, heroes) {
    heroesList.innerHTML = ""
    if(heroes == null || heroes.length == 0) {
        return
    }
    heroes.forEach( hero => {
        let row = document.createElement("div")
        row.classList.add("row")

        let heroTextContainer = document.createElement("div")
        heroTextContainer.classList = ["column column-50"]

        let heroText = document.createElement("div")
        heroText.innerHTML = `${hero.name} has the power of ${hero.power} !`

        heroTextContainer.appendChild(heroText)

        let buttonContainer = document.createElement("div")
        buttonContainer.classList = ["column column-50"]

        let deleteButton = document.createElement("button")
        deleteButton.innerHTML = "Delete"
        deleteButton.classList = ["button delete"]
        deleteButton.addEventListener("click", event => {
            delete_hero(channel, hero.name)
        })

        buttonContainer.appendChild(deleteButton)

        let editButton = document.createElement("button")
        editButton.innerHTML = "Edit"
        editButton.classList = ["button float-right"]
        editButton.addEventListener("click", event => {
            updateRow.classList.remove("hidden")
        })

        buttonContainer.appendChild(editButton)

        let updateRow = document.createElement("div")
        updateRow.classList = ["row hidden"]

        let updateNameInput = document.createElement("input")
        updateNameInput.classList = ["column column-40"]
        updateNameInput.type = "text"
        updateNameInput.placeholder = "New name"

        let updatePowerInput = document.createElement("input")
        updatePowerInput.classList = ["column column-40"]
        updatePowerInput.type = "text"
        updatePowerInput.placeholder = "New power"

        let updateHeroButton = document.createElement("button")
        updateHeroButton.classList = ["button column column-20"]
        updateHeroButton.innerHTML = "Update"
        updateHeroButton.addEventListener("click", event => {
            update_hero(channel,
                hero.name,
                updateNameInput.value,
                updatePowerInput.value
            )
        })

        updateRow.appendChild(updateNameInput)
        updateRow.appendChild(updatePowerInput)
        updateRow.appendChild(updateHeroButton)

        let hr = document.createElement("hr")

        row.appendChild(heroTextContainer)
        row.appendChild(buttonContainer)
        heroesList.appendChild(row)
        heroesList.appendChild(updateRow)
        heroesList.appendChild(hr)
    })
}

topicInput.addEventListener("keypress", event =>{
    if(event.keyCode == 13) {
        new_list(topicInput.value)
        topicInput.value = ""
    }
})

newButton.addEventListener("click", event => {
    let new_c = new_channel(topicInput.value)
    new_list(topicInput.value)
    topicInput.value = ""
})

submitHeroButton.addEventListener("click", event => {
    let name = heroNameInput.value
    let power = heroPowerInput.value

    add_hero(current_channel, name, power)

    heroNameInput.value = ""
    heroPowerInput.value = ""
})

saveButton.addEventListener("click", event => {
    if(current_channel) {
        current_channel.push("save_to_json")
    }
})

lobby.join()

export default socket
